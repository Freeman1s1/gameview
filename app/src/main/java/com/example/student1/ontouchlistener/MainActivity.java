package com.example.student1.ontouchlistener;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener{
    GameView scene;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scene=new GameView(this);
        scene.setOnTouchListener(this);
        setContentView(scene);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        float x=motionEvent.getX();
        float y=motionEvent.getY();

        /*switch(motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN : break;
            case MotionEvent.ACTION_MOVE : scene.setIsMoving(true);
                break;
            case MotionEvent.ACTION_UP : scene.setIsMoving(false);
                break;
        }*/
        scene.setTouchCoords(x, y);


        return true;
    }
}
