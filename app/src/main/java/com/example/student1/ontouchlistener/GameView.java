package com.example.student1.ontouchlistener;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;



/**
 * Created by student1 on 05.12.16.
 */

public class GameView extends View {
   private float touchX;
    private float touchY;
    private  boolean isMoving;
    private float x1, x2, y1, y2;
    private  float xspeed, yspeed;
    private float x,y,size;
    public GameView(Context context) {
        super(context);
        xspeed=10;
        yspeed=5;
        size=50;

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
       /* Paint pText=new Paint();
        pText.setColor(Color.RED);
        pText.setTextSize(24f);
        canvas.drawText("x = " + touchX + "\n" + "y = " + touchY, touchX, touchY, pText);
        Paint line=new Paint();
        line.setColor(Color.RED);
        line.setStrokeWidth(5f);*/
       //canvas.drawLine(x1, y1, x2, y2, line);
        canvas.drawRect(x-size, y-size, x+size, y+size, new Paint());
        x+=xspeed;
        y+=yspeed;

        if(x>canvas.getWidth() || x<0){
            xspeed=-xspeed;

        }
        if(y>canvas.getHeight() || y<0){
            yspeed=-yspeed;

        }
        invalidate();
    }




   /* public void setIsMoving(boolean isMoving){
        this.isMoving=isMoving;

    }*/
    public void setTouchCoords(float x, float y) {
       /*if(!isMoving){
            x1=x;
            y1=y;

        }
        else{
            x2=x;
            y2=y;
        }*/
        invalidate();

    }



}
